﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Xml;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //GetCompressedSvgFromPdf();

            //decompress();
            //DecompressFlateSvg();
            FormatFromPdfStyleToXmlStyle();

            //GetJpgFromPdf();
        }

        static void FormatFromPdfStyleToXmlStyle()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?> \n" +
                "<!DOCTYPE svg PUBLIC \" -//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" +
                "<svg  x=\"0\" y=\"0\" width=\"4071\" height=\"2823\" viewBox=\"0 0 4071 2823\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">" +// xmlns:xlink=\"http://www.w3.org/1999/xlink\" style=\"display: block;margin-left: auto;margin-right: auto;\"
                "<path d=\"M0, 0 L0, 2823 L4071, 2823 L4071, 0 Z \" fill=\"#FFFFFF\" stroke=\"none\"/>" +
                "<g> \n</g> \n</svg>"
);
            string _in;
            List<string> paths = null;
            List<string> xmlStylePaths = new List<string>();
            using (var decompressedPdfSvg = File.OpenText("sample-vector-out.svg"))//OPENS AS UTF-8
            {
                _in = decompressedPdfSvg.ReadToEnd();
            }
            ;

            var gNode = doc.GetElementsByTagName("g")[0];
            paths = _in.Split('f').ToList();
            paths[0] = paths[0].Split("gs")[1];
            paths.RemoveAt(paths.Count() - 1);
            foreach (var path in paths)
            {
                var index = 0;
                var nextIndex = 0;
                var builder = new StringBuilder();
                foreach (var symbol in path)
                {
                    if (symbol == 'm' || symbol == 'c' || symbol == 'l')
                    {
                        var upperSymbol = symbol == 'm' ? 'M' : symbol == 'c' ? 'C' : 'L';
                        builder.Insert(index, upperSymbol);
                        index = nextIndex + 1;
                    }
                    else if (symbol == 'h')
                    {
                        builder.Insert(index, 'Z');
                        index = nextIndex + 1;
                    }
                    else if (symbol == '\n')
                    {
                        builder.Append(' ');
                    }
                    else
                    {
                        builder.Append(symbol);
                    }

                    ++nextIndex;
                }

                var xmlStyledString = builder.ToString();
                XmlElement elem = doc.CreateElement("", "path", gNode.NamespaceURI);
                elem.SetAttribute("d", xmlStyledString);

                gNode.AppendChild(elem);
            }

            doc.Save("./sample-vector-out-xml.svg");
        }

        static void DecompressFlateSvg()
        {

            var directorySelected = new DirectoryInfo("./");

            var svgFileinfo = directorySelected.GetFiles("sample-vector.svg").FirstOrDefault();
            if (svgFileinfo == null)
                throw new ArgumentNullException($"no compressed files in directory {directorySelected.FullName}");

            var length = new long[] { svgFileinfo.Length * 2 };
            var _out = new byte[length[0]];

            var compressedSvgFile = File.OpenRead("./sample-vector.cmp");

            var _firstTwo = new byte[2];
            compressedSvgFile.Read(_firstTwo, 0, 2);

            var compressedLength = new long[] { compressedSvgFile.Length - 2 };
            var _in = new byte[compressedLength[0]];
            compressedSvgFile.Read(_in, 0, (int)compressedLength[0]);

            var decode = new Decode();

            try
            {
                var err = decode.puff(_out, length, _in, compressedLength);

                using (var decompressedTream = File.Create("sample-vector-out.svg"))
                {
                    decompressedTream.Write(_out, 0, (int)length[0]);
                }
                ;
            }
            catch (Exception ex)
            {
                ;
            }
            //var compressedSvgFileinfo = directorySelected.GetFiles("*.cmp").FirstOrDefault();
            //if (compressedSvgFileinfo == null)
            //    throw new ArgumentNullException($"no compressed files in directory {directorySelected.FullName}");
            //Decompress(compressedSvgFileinfo);
            ;
        }

        static void GetCompressedSvgFromPdf()
        {
            try
            {
                using (var pdfStream = File.OpenRead("sample-vector.pdf"))
                {
                    var pdfBuffer = new byte[pdfStream.Length];

                    pdfStream.Read(pdfBuffer, 0, (int)pdfStream.Length);

                    var i = 0;
                    var j = 0;
                    while (pdfBuffer[i] != 70 || pdfBuffer[i + 1] != 108 || pdfBuffer[i + 2] != 97
                        || pdfBuffer[i + 3] != 116 || pdfBuffer[i + 4] != 101)
                    {
                        ++i;
                    }
                        ;
                    while (pdfBuffer[i] != 115 || pdfBuffer[i + 1] != 116 || pdfBuffer[i + 2] != 114)
                    {
                        ++i;
                    }
                    i += 7;//including LF
                    ;
                    j = i;
                    while (pdfBuffer[j] != 101 || pdfBuffer[j + 1] != 110 || pdfBuffer[j + 2] != 100
                        || pdfBuffer[j + 3] != 115 || pdfBuffer[j + 4] != 116 || pdfBuffer[j + 5] != 114)
                    {
                        ++j;
                    }
                    j -= 1;//excluding LF
                    ;
                    using (var compressedSvgImgStream = File.OpenWrite("./sample-vector.cmp"))
                    {
                        compressedSvgImgStream.Write(pdfBuffer, i, j - i);
                    }
                    ;
                }
            }
            catch (Exception ex)
            {
                ;
            }
        }

        static void GetJpgFromPdf()
        {
            try
            {
                using (var pdfStream = File.OpenRead("./passport-Ivan-Zamkovets - Copy.pdf"))
                {
                    var pdfBuffer = new byte[pdfStream.Length];

                    pdfStream.Read(pdfBuffer, 0, (int)pdfStream.Length);

                    var i = 0;
                    var j = 0;
                    var k = 0;
                    while (pdfBuffer[i] != pdfStream.Length - 3)
                    {
                        while (pdfBuffer[i] != 68 || pdfBuffer[i + 1] != 67 || pdfBuffer[i + 2] != 84)
                        {
                            ++i;
                        }
                        ;
                        while (pdfBuffer[i] != 115 || pdfBuffer[i + 1] != 116 || pdfBuffer[i + 2] != 114)
                        {
                            ++i;
                        }
                        i += 7;//including LF
                        ;
                        j = i;
                        while (pdfBuffer[j] != 101 || pdfBuffer[j + 1] != 110 || pdfBuffer[j + 2] != 100
                            || pdfBuffer[j + 3] != 115 || pdfBuffer[j + 4] != 116 || pdfBuffer[j + 5] != 114)
                        {
                            ++j;
                        }
                        j -= 1;//excluding LF
                        ;
                        using (var jpegImgStream = File.OpenWrite($"./passport-Ivan-Zamkovets - Copy-{k}.jpeg"))
                        {
                            jpegImgStream.Write(pdfBuffer, i, j - i);
                        }
                    ;
                        ++k;
                    }
                }
            }
            catch (Exception ex)
            {
                ;
            }
        }

        private static void decompress()
        {
            var compressedSvgFile = File.OpenRead("./sample-vector.cmp");

            var _firstTwo = new byte[2];
            compressedSvgFile.Read(_firstTwo, 0, 2);

            var compressedLength = new long[] { compressedSvgFile.Length - 2 };
            var _in = new byte[compressedLength[0]];
            compressedSvgFile.Read(_in, 0, (int)compressedLength[0]);

            var stream = new MemoryStream();

            using (var compressStream = new MemoryStream(_in))
            using (var decompressor = new DeflateStream(compressStream, CompressionMode.Decompress))
                decompressor.CopyTo(stream);

            var result = Encoding.Default.GetString(stream.ToArray());

            return;
        }

        public static void Decompress(FileInfo fileToDecompress)
        {
            //System.IO.Compression.GZipStream

            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (DeflateStream decompressionStream = new DeflateStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine("Decompressed: {0}", fileToDecompress.Name);
                    }
                }
            }
        }
    }

    public class Decode
    {
        /*
 * Maximums for allocations and loops.  It is not useful to change these --
 * they are fixed by the deflate format.
 */
        public int MAXBITS => 15;            /* maximum bits in a code */
        public int MAXLCODES => 286;          /* maximum number of literal/length codes */
        public int MAXDCODES => 30;          /* maximum number of distance codes */
        public int MAXCODES => (MAXLCODES + MAXDCODES);  /* maximum codes lengths to read */
        public int FIXLCODES => 288; /* number of fixed literal/length codes */

        public class huffman
        {
            public int countIndex;//because of direct operations with pointers in C language

            public Int16[] count;       /* number of symbols of each length */
            public Int32[] symbol;//Int16[] symbol; error in construct() /* canonically ordered symbols */
        };

        /* input and output state */
        public class state
        {
            /* output state */
            public byte[] _out;         /* output buffer */
            public long outlen;//unsigned long IT'S A ?POINTER       /* available space at out */
            public long outcnt;//unsigned long IT'S A ?POINTER        /* bytes written to out so far */

            /* input state */
            public byte[] _in;    /* input buffer */
            public long inlen;//unsigned long IT'S A ?POINTER         /* available input at in */
            public long incnt;//unsigned long IT'S A ?POINTER         /* bytes read so far */
            public int bitbuf;                 /* bit buffer */
            public int bitcnt;                 /* number of bits in bit buffer */

            /* input limit error return state for bits() and decode() */
            //jmp_buf env;
        };

        public int puff(byte[] dest,           /* pointer to destination pointer */
         long[] destlen,        /* amount of output space */
         byte[] source,   /* pointer to source data pointer */
         long[] sourcelen)      /* amount of input available */
        {
            state s = new state();             /* input/output state */
            int last, type;             /* block information */
            int err = 0;                    /* return value */

            /* initialize output state */
            s._out = dest;
            s.outlen = destlen[0];//*destlen;                /* ignored if dest is NIL */
            s.outcnt = 0;

            /* initialize input state */
            s._in = source;
            s.inlen = sourcelen[0];//*sourcelen;
            s.incnt = 0;
            s.bitbuf = 0;
            s.bitcnt = 0;

            /* return if bits() or decode() tries to read past available input */
            //if (setjmp(s.env) != 0)             /* if came back here via longjmp() */
            //err = 2;                        /* then skip do-loop, return error */
            //else {
            try
            {
                /* process blocks until last block or error */
                do
                {
                    last = bits(s, 1);         /* one if last block */
                    type = bits(s, 2);         /* block type 0..3 */
                    err = type == 0 ?
                                stored(s) :
                                (type == 1 ?
                                _fixed(s) :
                        (type == 2 ?
                            dynamic(s) :
                            -1));       /* type == 3, invalid */
                    if (err != 0)
                        break;                  /* return with error */
                } while (last == 0);//!last
            }
            catch (Exception ex)
            {
                ;
            }
            //}

            /* update the lengths and return */
            if (err <= 0)
            {
                destlen[0] = s.outcnt;//* destlen
                sourcelen[0] = s.incnt;//* sourcelen
            }
            return err;
        }

        private int stored(state s)
        {
            UInt32 len;       /* length of stored block */

            /* discard leftover bits from current byte (assumes s->bitcnt < 8) */
            s.bitbuf = 0;
            s.bitcnt = 0;

            /* get length and check against its one's complement */
            if (s.incnt + 4 > s.inlen)
                return 2;                               /* not enough input */

            len = s._in[s.incnt++];
            len |= (UInt32)(s._in[s.incnt++] << 8);

            ;
            #region check
            var byteOne = s._in[s.incnt++];
            var byteTwo = s._in[s.incnt++];

            var lenByteOne = (~len & 0xff);
            var castedlenByteOne = (byte)(lenByteOne);

            var lenByteTwo = ((~len >> 8) & 0xff);
            var castedlenByteTwo = (byte)(lenByteTwo);

            s.incnt -= 2;
            #endregion
            ;
            if (s._in[s.incnt++] != (byte)(~len & 0xff) ||
                s._in[s.incnt++] != (byte)((~len >> 8) & 0xff))
                throw new Exception("stored: didn't match complement!");
            //return -2;                              /* didn't match complement! */

            /* copy len bytes from in to out */
            if (s.incnt + len > s.inlen)
                return 2;                               /* not enough input */

            if (s._out != null)
            {
                if (s.outcnt + len > s.outlen)
                    return 1;                           /* not enough output space */
                while (len != 0)//len--
                    s._out[s.outcnt++] = s._in[s.incnt++];
                --len;
            }
            else
            {                                      /* just scanning */
                s.outcnt += len;
                s.incnt += len;
            }

            /* done with a valid stored block */
            return 0;
        }

        private int _fixed(state s)
        {
            int virgin = 1;
            short[] lencnt = new short[MAXBITS + 1];
            Int32[] lensym = new Int32[FIXLCODES];//short[]
            short[] distcnt = new short[MAXBITS + 1];
            Int32[] distsym = new Int32[FIXLCODES];//short[]
            huffman lencode = new huffman();//static struct huffman lencode, distcode;
            huffman distcode = new huffman();

            /* build fixed huffman tables if first call (may not be thread safe) */
            if (virgin != 0)
            {
                int symbol;
                short[] lengths = new short[FIXLCODES];

                /* construct lencode and distcode */
                lencode.count = lencnt;
                lencode.symbol = lensym;
                distcode.count = distcnt;
                distcode.symbol = distsym;

                /* literal/length table */
                for (symbol = 0; symbol < 144; symbol++)
                    lengths[symbol] = 8;

                for (; symbol < 256; symbol++)
                    lengths[symbol] = 9;

                for (; symbol < 280; symbol++)
                    lengths[symbol] = 7;

                for (; symbol < FIXLCODES; symbol++)
                    lengths[symbol] = 8;

                construct(lencode, lengths, 0, FIXLCODES);//&lencode

                /* distance table */
                for (symbol = 0; symbol < MAXDCODES; symbol++)
                    lengths[symbol] = 5;

                construct(distcode, lengths, 0, MAXDCODES);//&distcode

                /* do this just once */
                virgin = 0;
            }

            /* decode data until end-of-block code */
            return codes(s, lencode, distcode);//&lencode, &distcode
        }

        private int dynamic(state s)
        {
            int nlen, ndist, ncode;             /* number of lengths in descriptor */
            int index;                          /* index of lengths[] */
            int err;                            /* construct() return value */
            short[] lengths = new short[MAXCODES];            /* descriptor code lengths */

            short[] lencnt = new short[MAXBITS + 1]; /* lencode memory */
            Int32[] lensym = new Int32[MAXLCODES];//short[] 
            short[] distcnt = new short[MAXBITS + 1]; /* distcode memory */
            Int32[] distsym = new Int32[MAXLCODES];//short[]

            huffman lencode = new huffman();//struct huffman lencode, distcode; /* length and distance codes */
            huffman distcode = new huffman();

            short[] order = //[19]     /* permutation of code length codes */
                {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};

            /* construct lencode and distcode */
            lencode.count = lencnt;
            lencode.symbol = lensym;
            distcode.count = distcnt;
            distcode.symbol = distsym;

            /* get number of lengths in each table, check lengths */
            nlen = bits(s, 5) + 257;
            ndist = bits(s, 5) + 1;
            ncode = bits(s, 4) + 4;

            if (nlen > MAXLCODES || ndist > MAXDCODES)
                return -3;                      /* bad counts */

            /* read code length code lengths (really), missing lengths are zero */
            for (index = 0; index < ncode; index++)
                lengths[order[index]] = (short)bits(s, 3);//(short) explicit conversion

            for (; index < 19; index++)
                lengths[order[index]] = 0;

            /* build huffman table for code lengths codes (use lencode temporarily) */
            err = construct(lencode, lengths, 0, 19);//&lencode, 0 is zero shift from array beginning

            if (err != 0)               /* require complete code set here */
                return -4;

            /* read length/literal and distance code length tables */
            index = 0;
            while (index < nlen + ndist)
            {
                int symbol;             /* decoded value */
                int len;                /* last length to repeat */

                symbol = decode(s, lencode);//&lencode

                if (symbol < 0)
                    return symbol;          /* invalid symbol */

                if (symbol < 16)                /* length in 0..15 */
                    lengths[index++] = (short)symbol;//lenghts required short, got int, did explicit cast

                else
                {                          /* repeat instruction */
                    len = 0; /* assume repeating zeros */

                    if (symbol == 16)
                    {         /* repeat last length 3..6 times */
                        if (index == 0)
                            return -5;          /* no last length! */

                        len = lengths[index - 1];       /* last length */
                        symbol = 3 + bits(s, 2);
                    }
                    else if (symbol == 17)      /* repeat zero 3..10 times */
                        symbol = 3 + bits(s, 3);
                    else                        /* == 18, repeat zero 11..138 times */
                        symbol = 11 + bits(s, 7);

                    if (index + symbol > nlen + ndist)
                        return -6;              /* too many lengths! */

                    while (symbol != 0)            /* repeat last or zero symbol times */
                    {
                        lengths[index++] = (short)len;//lenghts required short, got int, did explicit cast

                        --symbol;
                    }
                }
            }

            /* check for end-of-block code -- there better be one! */
            if (lengths[256] == 0)
                return -9;

            /* build huffman table for literal/length codes */
            err = construct(lencode, lengths, 0, nlen);//&lencode, 0 is zero shift from array beginning

            if (err != 0 && (err < 0 || nlen != lencode.count[0] + lencode.count[1]))
                return -7;      /* incomplete code ok only for single length 1 code */

            /* build huffman table for distance codes */
            err = construct(distcode, lengths, nlen, ndist);//&distcode, lengths + nlen, nlen is shift from array beginning

            if (err != 0 && (err < 0 || ndist != distcode.count[0] + distcode.count[1]))
                return -8;      /* incomplete code ok only for single length 1 code */

            /* decode data until end-of-block code */
            return codes(s, lencode, distcode);//&lencode, &distcode
        }

        private int codes(state s,
                huffman lencode,
                huffman distcode)
        {
            int symbol;         /* decoded symbol */
            int len;            /* length for copy */
            UInt32 dist;      /* distance for copy */

            short[] lens = {//[29] /* Size base for length codes 257..285 */
        3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31,
        35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258};

            short[] lext = {//[29] /* Extra bits for length codes 257..285 */
        0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2,
        3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0};

            short[] dists = {//[30] /* Offset base for distance codes 0..29 */
        1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193,
        257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145,
        8193, 12289, 16385, 24577};

            short[] dext = {//[30] /* Extra bits for distance codes 0..29 */
        0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6,
        7, 7, 8, 8, 9, 9, 10, 10, 11, 11,
        12, 12, 13, 13};

            /* decode literals and length/distance pairs */
            do
            {
                symbol = decode(s, lencode);
                if (symbol < 0)
                    return symbol;              /* invalid symbol */

                if (symbol < 256)
                {             /* literal: symbol is the byte */
                    /* write out the literal */
                    if (s._out != null)
                    {//s->out != NIL, byte value with address of out is not free/empty

                        if (s.outcnt == s.outlen)
                            return 1;

                        s._out[s.outcnt] = (byte)symbol;//s->out[s->outcnt] = symbol; INT IS PUT TO CHAR * address with shift in bytes, NOT ints
                    }

                    s.outcnt++;//s->outcnt++; outcnt IS ADDRESS in memory
                }
                else if (symbol > 256)
                {        /* length */
                    /* get and compute length */
                    symbol -= 257;

                    if (symbol >= 29)
                        return -10;             /* invalid fixed code */

                    len = lens[symbol] + bits(s, lext[symbol]);

                    /* get and check distance */
                    symbol = decode(s, distcode);

                    if (symbol < 0)
                        return symbol;          /* invalid symbol */

                    dist = (uint)(dists[symbol] + bits(s, dext[symbol]));//explicit cast equired
                                                                         //#ifndef INFLATE_ALLOW_INVALID_DISTANCE_TOOFAR_ARRR
                    if (dist > s.outcnt)//s->outcnt
                        return -11;     /* distance too far back */
                    //#endif

                    /* copy length bytes from distance bytes back */
                    if (s._out != null)
                    {//s->out != NIL
                        if (s.outcnt + len > s.outlen)//s->outcnt + len > s->outlen
                            return 1;

                        while (len != 0)
                        {
                            s._out[s.outcnt] =//s->out[s->outcnt]
                                              //#ifdef INFLATE_ALLOW_INVALID_DISTANCE_TOOFAR_ARRR
                                dist > s.outcnt ? //s->outcnt
                                (byte)0 : s._out[s.outcnt - dist];//s->out[s->outcnt - dist]; 
                                                                  //#endif

                            s.outcnt++;//s->outcnt++;

                            --len;
                        }
                    }
                    else
                        s.outcnt += len;//s->outcnt += len;
                }
            } while (symbol != 256);            /* end of block symbol */

            /* done with a valid fixed or dynamic block */
            return 0;
        }

        private int decode(state s, huffman h)//fast version
        {
            int len;            /* current number of bits in code */
            int code;           /* len bits being decoded */
            int first;          /* first code of length len */
            int count;          /* number of codes of length len */
            int index;          /* index of first code of length len in symbol table */
            int bitbuf;         /* bits from stream */
            int left;           /* bits left in next or left to process */
            int next;//short *      /* next number of codes */

            bitbuf = s.bitbuf;
            left = s.bitcnt;
            code = first = index = 0;
            len = 1;
            next = h.countIndex + 1;//h->count + 1;

            while (true)
            {//1
                while (left != 0)
                {
                    code |= bitbuf & 1;
                    bitbuf >>= 1;
                    count = h.count[next];// *next++;
                    ++next;

                    if (code - count < first)
                    { /* if length len, return symbol */
                        s.bitbuf = bitbuf;
                        s.bitcnt = (s.bitcnt - len) & 7;
                        return h.symbol[index + (code - first)];
                    }

                    index += count;             /* else update for next length */
                    first += count;
                    first <<= 1;
                    code <<= 1;
                    len++;

                    --left;
                }

                left = (MAXBITS + 1) - len;
                if (left == 0)
                    break;

                if (s.incnt == s.inlen)
                    throw new Exception("longjmp(s->env, 1); out of input");//longjmp(s->env, 1);         /* out of input */

                bitbuf = s._in[s.incnt++];

                if (left > 8)
                    left = 8;
            }
            return -10;                         /* ran out of codes */
        }

        private int construct(huffman h, short[] length, int lengthCurrentIndex, int n)//int lengthCurrentIndex added as length was pointer in C code
        {
            int symbol;         /* current symbol when stepping through length[] */
            int len;            /* current length when stepping through h->count[] */
            int left;           /* number of possible codes left of current length */
            short[] offs = new short[MAXBITS + 1];      /* offsets in symbol table for each length */

            /* count number of codes of each length */
            for (len = 0; len <= MAXBITS; len++)
                h.count[len] = 0;

            for (symbol = 0; symbol < n; symbol++)
                (h.count[length[symbol + lengthCurrentIndex]])++;   /* assumes lengths are within bounds */

            if (h.count[0] == n)               /* no codes! */
                return 0;                       /* complete, but decode() will fail */

            /* check for an over-subscribed or incomplete set of lengths */
            left = 1;                           /* one possible code of zero length */
            for (len = 1; len <= MAXBITS; len++)
            {
                left <<= 1;                     /* one more bit, double codes left */
                left -= h.count[len];          /* deduct count from possible codes */

                if (left < 0)
                    return left;                /* over-subscribed--return negative */
            }                                   /* left > 0 means incomplete */

            /* generate offsets into symbol table for each length for sorting */
            offs[1] = 0;
            for (len = 1; len < MAXBITS; len++)
                offs[len + 1] = (short)(offs[len] + h.count[len]);//was not (short)

            /*
             * put symbols in table sorted by length, by symbol order within each
             * length
             */
            for (symbol = 0; symbol < n; symbol++)
                if (length[symbol + lengthCurrentIndex] != 0)
                    h.symbol[offs[length[symbol + lengthCurrentIndex]]++] = symbol;

            /* return zero for complete set, positive for incomplete set */
            return left;
        }

        private int bits(state s, int need)
        {
            long val;           /* bit accumulator (can use up to 20 bits) */

            /* load at least need bits into val */
            val = s.bitbuf;//s->bitbuf;
            while (s.bitcnt < need)//s->bitcnt
            {
                if (s.incnt == s.inlen)//s->incnt == s->inlen
                    throw new Exception("longjmp(s->env, 1), out of input");//longjmp(s->env, 1);         /* out of input */

                val |= (long)(s._in[s.incnt++]) << s.bitcnt; //(long)(s->in[s->incnt++]) << s->bitcnt;  /* load eight bits */
                s.bitcnt += 8;//s->bitcnt
            }

            /* drop need bits and update buffer, always zero to seven bits left */
            s.bitbuf = (int)(val >> need);//s->bitbuf
            s.bitcnt -= need;//s->bitcnt

            /* return need bits, zeroing the bits above that */
            return (int)(val & ((1L << need) - 1));
        }
    }
}
